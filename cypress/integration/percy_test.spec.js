/// <reference types="cypress" />

const widths = [800, 1280, 1920]

const URL = 'http://localhost:5501/app_v2/'

describe('Visual Regression', () => {
    it(`Snapshots should match`, () => {
        cy.visit(URL + 'signup.html')
        cy.percySnapshot(`Signup`)
        cy.visit(URL + 'signin.html')
        cy.percySnapshot(`Signin`)
        cy.visit(URL + 'index.html')
        cy.wait(2000)
        cy.percySnapshot(`Dashboard`)
        cy.get('.sidebar .sidebar-toggle').click({force:true})
        cy.get('#theme_button').click({force:true})
        cy.get('#notification_button').click({force:true})
        cy.get('[type="checkbox"]').eq(1).check({force:true})
        cy.get('[type="checkbox"]').eq(2).check({force:true})
        cy.percySnapshot(`Active Dashboard`)
    })
})