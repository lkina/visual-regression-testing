/// <reference types="cypress" />

const sizes = [
    //'ipad-2'
    //'iphone-x',
    //'samsung-note9',
    //[800, 600],
    [1920, 1080]
    ]

const URL = 'http://localhost:5501/app_v2/'

describe('Visual Regression', () => {
  sizes.forEach(size => {
    it(`Signup should match in resolution ${size}`, () => {
        cy.setResolution(size)
        cy.visit(URL + 'signup.html')
        cy.matchImageSnapshot(`Signup`)
    })

    it(`Signin should match in resolution ${size}`, () => {
        cy.setResolution(size)
        cy.visit(URL + 'signin.html')
        cy.matchImageSnapshot(`Signin`)
    })

    it(`Dashboard should match in resolution ${size}`, () => {
        cy.setResolution(size)
        cy.visit(URL + 'index.html')
        cy.wait(2000)
        cy.matchImageSnapshot(`Dashboard`)
        cy.get('.sidebar .sidebar-toggle').click()
        cy.get('#theme_button').click({force:true})
        cy.get('#notification_button').click({force:true})
        cy.get('[type="checkbox"]').eq(1).check({force:true})
        cy.get('[type="checkbox"]').eq(2).check({force:true})
        cy.matchImageSnapshot(`Active Dashboard`)
    })
})
})