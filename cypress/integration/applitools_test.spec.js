/// <reference types="cypress" />

const URL = 'http://localhost:5501/app_v2/'

describe('Visual Regression', () => {
  
    it(`Snapshots should match`, () => {
        cy.visit(URL + 'signup.html')
        cy.eyesOpen({
            appName: 'Elegant',
            testName: 'Visual Batch',
            browser: [
                { width: 1920, height:1080, name: 'chrome' },
                //{ width: 1920, height:1080, name: 'firefox' },
                // { width: 800, height:600, name: 'chrome' },
                // { deviceName: 'iPhone X' }
            ]
        })
        cy.eyesCheckWindow(`Signup`)
        cy.visit(URL + 'signin.html')
        cy.eyesCheckWindow(`Signin`)
        cy.visit(URL + 'index.html')
        cy.wait(2000)
        cy.eyesCheckWindow(`Dashboard`)
        cy.get('.sidebar .sidebar-toggle').click({force:true})
        cy.get('#theme_button').click({force:true})
        cy.get('#notification_button').click({force:true})
        cy.get('[type="checkbox"]').eq(1).check({force:true})
        cy.get('[type="checkbox"]').eq(2).check({force:true})
        cy.wait(3000)
        cy.eyesCheckWindow(`Active Dashboard`)
        cy.eyesClose()
    })
})






// it(`Signup should match`, () => {
//     cy.visit(URL + 'signup.html')
//     cy.eyesOpen({
//         appName: 'Elegant',
//         testName: 'Signup',
//         browser: { width: 1920, height:1080}
//     })
//     cy.eyesCheckWindow(`Signup`)
//     cy.eyesClose()
// })

// it(`Signin should match`, () => {
//     cy.visit(URL + 'signin.html')
//     cy.eyesOpen({
//         appName: 'Elegant',
//         testName: 'Signin',
//         browser: { width: 1920, height:1080}
//     })
//     cy.eyesCheckWindow(`Signin`)
//     cy.eyesClose()
// })

// it(`Dashboard should match`, () => {
//     cy.visit(URL + 'index.html')
//     cy.wait(2000)
//     cy.eyesOpen({
//         appName: 'Elegant',
//         testName: 'Dashboard',
//         browser: { width: 1920, height:1080}
//     })
//     cy.eyesCheckWindow(`Dashboard`)
//     cy.get('.sidebar .sidebar-toggle').click()
//     cy.get('#theme_button').click({force:true})
//     cy.get('#notification_button').click({force:true})
//     cy.get('[type="checkbox"]').eq(1).check({force:true})
//     cy.get('[type="checkbox"]').eq(2).check({force:true})
//     cy.eyesCheckWindow(`Active Dashboard`)
//     cy.eyesClose()
// })