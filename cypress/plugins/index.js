/// <reference types="cypress" />
// @ts-check

// Cypress Plugin
const { addMatchImageSnapshotPlugin } = require('cypress-image-snapshot/plugin')

module.exports = (on, config) => {
  addMatchImageSnapshotPlugin(on, config)
}

// @ts-ignore
require('@applitools/eyes-cypress')(module);
