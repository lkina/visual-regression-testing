# Visual regression testing

This repository contains 3 ways for applying visual regression testing with cypress: cypress-image-snapshot plugin, Percy integration, and Applitools integration.

**Instructions:**

1. To use Percy or Applitools integration, you need an account in [percy.io](https://percy.io/) or [apllitools.com](https://applitools.com/).
2. Install dependencies.
3. Then add the API key to the session:

```
export PERCY_TOKEN=<your token here>
export APPLITOOLS_API_KEY=<your token here>
```

Run tests with Cypress Plugin

```
npm run cy:plugin
npm run cy:update
```

Run tests with Cypress + Percy

```
npm run cy:percy
```

Run tests with Cypress + Applitools

```
npm run cy:applitools
```
